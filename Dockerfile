FROM ubuntu:22.04

SHELL ["/bin/bash", "-c"]

# Install prerequisites

RUN apt update -y && \
    apt install -y curl git perl python2 python3 python3-pip gengetopt \
    make autoconf g++ flex bison ccache libgoogle-perftools-dev numactl \
    perl-doc libfl2 libfl-dev zlib1g zlib1g-dev gawk libncurses5 help2man

RUN ln -s /usr/bin/python2 /usr/bin/python
RUN pip3 install seaborn pandas numpy

# Build latest Verilator from sources

WORKDIR /tools/

RUN git clone https://github.com/verilator/verilator
ENV VERILATOR_ROOT="/tools/verilator/"

WORKDIR ${VERILATOR_ROOT}

RUN git checkout v4.228
RUN autoconf && ./configure --prefix=${VERILATOR_ROOT}/build/ && \
    make -j$(nproc) && make install
ENV PATH="${VERILATOR_ROOT}/build/bin/:${PATH}"
ENV VERILATOR_HOME="${VERILATOR_ROOT}/build/"

# Install riscv32 gcc with PULP support

WORKDIR /tools/

ENV RISCV_GCC_ROOT="/tools/riscv-gcc/"

RUN curl -L -O spclstorage.inf.ethz.ch/~digirols/pspin/riscv-gcc-ubuntu.tar.gz && \
    tar -xf /tools/riscv-gcc-ubuntu.tar.gz -C /tools/ && \
    chmod 777 -R ${RISCV_GCC_ROOT} && \
    rm /tools/riscv-gcc-ubuntu.tar.gz
